# Ukuli Work Desktop Client
Desktop client for [Ukuli Work](https://github.com/TomiToivio/ukuli-work)

## Desktop Client branches
* Master (Distribution version, should be the same as production).
* Production (Ready for production, merge with master)
* Staging (The one we run various tests against)
* Development (Here we merge the issue branches)
